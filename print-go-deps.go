/*
 * Takes a number of paths that are go binaries and prints
 * all the modules that the binary depends with the format
 * '$path: $module'.
 *
 * Error is printed out if it fails to read the binary (like
 * a path that does not exist) and nothing is returned if the
 * binaries have no dependencies
 */
package main

import (
	"debug/buildinfo"
	"fmt"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		os.Exit(99)
	}

	for _, arg := range os.Args[1:] {
		info, err := buildinfo.ReadFile(arg)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
			continue
		}

		if info.Deps == nil {
			continue
		}

		for x := range info.Deps {
			fmt.Printf("%s: %s\n", arg, info.Deps[x].Path)
		}
	}
}
