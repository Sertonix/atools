#!/usr/bin/env ash
# shellcheck shell=ash

lint_msg() {
    local msg="$1" tag="$2" severity="$3"

    printf '%s:[%s]:%s:%s\n' "$severity" "$tag" "$service" "$msg"
}

scan() {
	local rx="$1" msg="$2" tag="$3" severity="$4"
	grep -E -Hn -e "$rx" "$service" |
		sed "s/^\([^:]*:[^:]*:\)\(.*\)/$severity:[$tag]:\1$msg/"
}

unexpected_shebang_line() {
    shebang=$(awk 'NR == 1' "$service")

    if [ "$shebang" != "#!/sbin/openrc-run" ]; then
        lint_msg "openrc init files should use '#!/sbin/openrc-run' as shebang line" "AL33" "IC"
    fi
}

custom_start_stop_function() {
    scan '\b(start|stop)\s*\(\)\s*\{' "Don't define a custom start\/stop function" 'AL34' 'IC'
}

_ret=0
for service; do
	if [ ! -f "$service" ]; then
		echo "error: no such file: '$service'"
		exit 1
	fi

	unexpected_shebang_line
	custom_start_stop_function
done | sort -t: -V | grep . && _ret=1
exit $_ret
