alint(5)

# NAME

alint - linting labels and tags

# DESCRIPTION

A label and a tag are 2 attributes given to each test in alint, a label is composed
by a collection of words separated by dashes and it is meant to convey the general
idea of what the test does. A tag is a string with AL followed by an integer, those
being assigned in increasing order as new tests are made but never re-using old ones.

tests can be skipped by setting an environment variable composed of SKIP_ + the label
in uppercase with the dashes replaced by underscore or by setting an environment variable
composed of SKIP_ + the tag.

# Severity

Severity is assigned to each tag and indicates how serious the violation found is:

## Serious (S)

Will cause problems during build or when the package is delivered to users. Should be fixed
immediately.

## Important (I)

Might cause problems during build or when the package is delivered to users in certain
situations. Should be fixed soon.

## Minor (M)

Won't directly cause issues during build or when the package is delivered to users.
Should be fixed when convenient.

This also holds style issues that are dictated on the CODINGSTYLE.md file.

## STYLE (T)

Won't cause any issues, are specific style issues pertinent to a concerned developer
or contributor. the _APKBUILD\_STYLE_ variable can be set to a specific value to enable
a subset of checks that the specific developer or user uses.

When a check belongs to this category this manual page also provides information on which
developers use a specific style check as developers can sometimes both want to have the
same check done on their own style.

Current valid values are:

- leo (style linting for leo)

# Certainty

Certainty is assigned to each tag and indicates how certain a test is that the violation
found is not a false positive

## Certain (C)

The test is certain that this is not a false positive

## Possible (P)

The test is not certain that this is a false positive, but there is a high chance it isn't

# TABLE

The following is a table of every tag, along with its tag, label, severity,
certainty, along with linter (which binary checks for it) and fixer (whether
apkbuild-fixer can fix it automatically).

[[ *tag*
:- _label_
:- _severity_
:- _certainty_
:- _linter_
:- _fixer_
|  *AL1*
:- default-builddir-value
:- Minor
:- Certain
:- apkbuild-lint
:- YES
|  *AL2*
:- unnecessary-return-1
:- Minor
:- Certain
:- apkbuild-lint
:- YES
|  *AL3*
:- pkgname-quoted
:- Minor
:- Certain
:- apkbuild-lint
:- NO
|  *AL4*
:- pkgver-quoted
:- Minor
:- Certain
:- apkbuild-lint
:- NO
|  *AL5*
:- empty-variable
:- Minor
:- Certain
:- apkbuild-lint
:- YES
|  *AL6*
:- custom-variable
:- Important
:- Certain
:- apkbuild-lint
:- NO
|  *AL7*
:- indent-tabs
:- Important
:- Certain
:- apkbuild-lint
:- NO
|  *AL8*
:- trailing-whitespace
:- Important
:- Certain
:- apkbuild-lint
:- YES
|  *AL9*
:- function-keyword
:- Serious
:- Certain
:- apkbuild-lint
:- NO
|  *AL10*
:- space-before-function-parenthesis
:- Minor
:- Certain
:- apkbuild-lint
:- YES
|  *AL11*
:- space-after-function-parenthesis
:- Minor
:- Certain
:- apkbuild-lint
:- YES
|  *AL12*
:- newline-opening-brace
:- Minor
:- Certain
:- apkbuild-lint
:- NO
|  *AL13*
:- superfluous-cd-builddir
:- Minor
:- Possible
:- apkbuild-lint
:- YES
|  *AL14*
:- pkgname-has-uppercase
:- Serious
:- Certain
:- apkbuild-lint
:- NO
|  *AL15*
:- pkgver-has-pkgrel
:- Serious
:- Certain
:- apkbuild-lint
:- NO
|  *AL16*
:- upper-repo-depends
:- Serious
:- Certain
:- aports-lint
:- N/A
|  *AL17*
:- duplicate-depends
:- Minor
:- Certain
:- aports-lint
:- N/A
|  *AL18*
:- upper-repo-makedepends
:- Serious
:- Certain
:- aports-lint
:- N/A
|  *AL19*
:- duplicate-makedepends
:- Minor
:- Certain
:- aports-lint
:- N/A
|  *AL20*
:- upper-repo-makedepends
:- Serious
:- Certain
:- aports-lint
:- N/A
|  *AL21*
:- duplicate-makedepends
:- Minor
:- Certain
:- aports-lint
:- N/A
|  *AL22*
:- duplicate-package
:- Serious
:- Certain
:- aports-lint
:- N/A
|  *AL23*
:- pkgname-dirname-mismatch
:- Important
:- Certain
:- aports-lint
:- N/A
|  *AL24*
:- depends-makedepends-checkdepends-overlap
:- Important
:- Certain
:- aports-lint
:- N/A
|  *AL25*
:- backticks-usage
:- Serious
:- Possible
:- apkbuild-lint
:- NO
|  *AL26*
:- \_builddir-is-set
:- Serious
:- Certain
:- apkbuild-lint
:- NO
|  *AL28*
:- literal-integer-is-quoted
:- Minor
:- Certain
:- apkbuild-lint
:- NO
|  *AL29*
:- pkgname-used-in-source
:- Minor
:- Certain
:- apkbuild-lint
:- YES
|  *AL30*
:- double-underscore-in-variable
:- Minor
:- Certain
:- apkbuild-lint
:- NO
|  *AL31*
:- variable-capitalized
:- Minor
:- Certain
:- apkbuild-lint
:- NO
|  *AL32*
:- braced-variable
:- Minor
:- Certain
:- apkbuild-lint
:- YES
|  *AL33*
:- unexpected-shebang-line
:- Important
:- Certain
:- initd-lint
:- N/A
|  *AL34*
:- custom-start-stop-function
:- Important
:- Certain
:- initd-lint
:- N/A
|  *AL35*
:- cpan-variable
:- Minor
:- Certain
:- apkbuild-lint
:- NO
|  *AL36*
:- overwrite-xflags
:- Serious
:- Certain
:- apkbuild-lint
:- NO
|  *AL37*
:- secfixes-missing-colon
:- Serious
:- Certain
:- secfixes-check
:- N/A
|  *AL38*
:- pkgver-pkgrel-missing-colon
:- Serious
:- Certain
:- secfixes-check
:- N/A
|  *AL39*
:- pkgver-pkgrel-invalid-pkgver
:- Serious
:- Certain
:- secfixes-check
:- N/A
|  *AL40*
:- pkgver-pkgrel-invalid-pkgrel
:- Serious
:- Certain
:- secfixes-check
:- N/A
|  *AL41*
:- security-identifier-missing-hyphen
:- Serious
:- Certain
:- secfixes-check
:- N/A
|  *AL47*
:- security-identifier-wrong-indent
:- Serious
:- Certain
:- secfixes-check
:- N/A
|  *AL48*
:- pkgver-pkgrel-wrong-indent
:- Serious
:- Certain
:- secfixes-check
:- N/A
|  *AL49*
:- invalid-option
:- Minor
:- Certain
:- apkbuild-lint
:- YES
|  *AL50*
:- cve-identifier-formatted-incorrectly
:- Minor
:- Certain
:- secfixes-check
:- N/A
|  *AL51*
:- gnutls-sa-identifier-formatted-incorrectly
:- Minor
:- Certain
:- secfixes-check
:- N/A
|  *AL52*
:- unknown-security-identifier
:- Minor
:- Certain
:- secfixes-check
:- N/A
|  *AL53*
:- xsa-identifier-formatted-incorrectly
:- Minor
:- Certain
:- secfixes-check
:- N/A
|  *AL54*
:- missing-default-prepare
:- Serious
:- Certain
:- apkbuild-lint
:- NO
|  *AL56*
:- missing-patch-description
:- Minor
:- Certain
:- aports-lint
:- N/A
|  *AL57*
:- invalid-arch
:- Serious
:- Certain
:- apkbuild-lint
:- YES
|  *AL58*
:- deprecated-packages
:- Serious
:- Certain
:- aports-lint
:- N/A
|  *AL59*
:- duplicate-identifier-value
:- Serious
:- Certain
:- secfixes-check
:- N/A
|  *AL61*
:- bad-version
:- Important
:- Certain
:- apkbuild-lint
:- NO
|  *AL62*
:- volatile-source
:- Serious
:- Certain
:- apkbuild-lint
:- NO
|  *AL63*
:- ghsl-identifier-formatted-incorrectly
:- Minor
:- Certain
:- secfixes-check
:- NO

# LABELS TAGS (aports-lint)

The following labels and tags are used by the *aports-lint* program.

## duplicate-package [AL22]

The Package being introduced is already present in another repo. Solve the
conflict by:

- If the package in the uppermost repo has more recent changes, merge them.
- Then delete the package in the upper repo.

Duplicate packages will shadow each other in the repo and the one with the
higher version will win, it can also cause programs to compile against
the wrong version of a package. E.g: if *main/foo-1-r0* and *testing/foo-2-r0* exist
and *main/bar* is updated to version 2 which requires *foo>=2-r0* then it will
fail because it will only find *main/foo-1-r0* and not *testing/foo-2-r0*.

Severity: Serious, Certainty: Certain

## upper-repo-depends [AL16]

The package depends on a package in a upper repo. The package must be moved to 
the upper repo or the dependency moved to the repo the package is.

Packages cannot depend on a package on an upper repo. Packages in main cannot
depend on packages in other repos. Packages in community can depend on main
and itself only. Packages in testing can depend on main, community and itself
only. Packages in unmaintained can depend on package on any repo but non-free.

Severity: Serious, Certainty: Certain

## duplicate-depends [AL17]

The APKBUILD has duplicate depends. One of them must be removed.

Declaring duplicate dependencies is superfluous.

Severity: Minor, Certainty: Certain

## upper-repo-makedepends [AL18]

The package makedepends on a package in a upper repo. The package must be moved
to the upper repo or the dependency moved to the repo the package is.

Packages cannot makedepend on a package on an upper repo. Packages in main cannot
makedepend on packages in other repos. Packages in community can depend on main
and itself only. Packages in testing can makedepend on main, community and itself
only. Packages in unmaintained can makedepend on packages of any repo but non-free.

Severity: Serious, Certainty: Certain

## duplicate-makedepends [AL19]

The APKBUILD has duplicate makedepends. One of them must be removed.

Declaring duplicate dependencies is superfluous.

Severity: Minor, Certainty: Certain

## upper-repo-checkdepends [AL20]

The package checkdepends on a package in a upper repo. The package must be moved
to the upper repo or the dependency moved to the repo the package is.

Packages cannot checkdepend on a package on an upper repo. Packages in main cannot
checkdepend on packages in other repos. Packages in community can checkdepend on main
and itself only. Packages in testing can checkdepend on main, community and itself
only. Packages in unmaintained can checkdepend on package on any repo but non-free.

Severity: Serious, Certainty: Certain

## duplicate-checkdepends [AL21]

The APKBUILD has duplicate checkdepends. One of them must be removed.

Declaring duplicate dependencies is superfluous.

Severity: Minor, Certainty: Certain

## pkgname-dirname-mismatch [AL23]

The pkgname variable of the APKBUILD has value *foo* but the directory in which the
APKBUILD is found is not named *foo*

Severity: Important, Certainty: Certain

## depends-makedepends-checkdepends-overlap [AL24]

A package is present in 2 to 3 of the 3 types of following dependencies: depends,
makedepends and checkdepends. All of them are installed during creation of the package,
please specify only once in the lowest common denominator location.

Severity: Important, Certainty: Certain

## deprecated-packages [AL58]

A package is present in depends, makedepends or checkdepends that is considered deprecated.

The deprecated packages are hard-coded into aports-lint with the option of adding custom ones
via CUSTOM_DEPRECATED_PACKAGES variable. They are considered no longer fit for usage in Alpine
Linux and should be removed IMMEDIATELY.

Severity: Serious, Certainty: Certain

## missing-patch-description [AL56]

A patch specified in `$sources` is missing a description. The
description should at the very least explain why the patch is necessary.

Severity: Minor, Certainty: Certain

# LABELS TAGS (apkbuild-lint)

The following labels and tags are used by the *apkbuild-lint* program

## default-builddir-value [AL1]

The value of builddir matches the default of *$srcdir/$pkgname-$pkgver*.
The *builddir* declaration can be removed.

Starting with v2.29.0 (Alpine version 3.3) of *abuild* the value is set automatically.

Some packages are excluded from this as they are built by abuild during
bootstrap and as such are built with the pkgname plus the -bootstrap suffix.

More packages can be added to the exceptions by passing a whitespace-separated
list in the variable CUSTOM_BOOTSTRAP_PACKAGES.

Severity: Minor, Certainty: Certain

## unnecessary-return-1 [AL2]

The APKBUILD has *|| return 1* statements. They can be safely removed.

Starting with version v2.15.0 of *abuild* the building process is executed
with *set -e* effectively adding a *|| return 1* to every command.

Severity: Minor, Certainty: Certain

## pkgname-quoted [AL3]

The APKBUILD's *pkgname* variable is quoted. It must not be quoted.

Severity: Minor, Certainty: Certain

## pkgver-quoted [AL4]

The APKBUILD's *pkgver* variable is quoted. It must not be quoted.

Severity: Minor, Certainty: Certain

## empty-variable [AL5]

The APKBUILD has variables that are empty values, they can safely be removed.

Empty variables can be removed to make the APKBUILD smaller and more
concise.

Severity: Minor, Certainty: Certain

## custom-variable [AL6]

The APKBUILD has custom variables that are not prefixed with an underscore.
prefix the variables with underscore.

Variables that do no affect behavior of *abuild* should be prefixed with an
underscore so maintainers and contributors can easily distinguish their
importance.

Severity: Important, Certainty: Certain

## indent-tabs [AL7]

The APKBUILD is using spaces instead of tabs for indenting. Replace the
spaces with tabs.

APKBUILDs use tab characters (\t) not spaces for indentation.

Severity: Important, Certainty: Certain

## trailing-whitespace [AL8]

The APKBUILD has trailing whitespace characters. Remove them.

Trailing whitespace is superfluous.

Severity: Important, Certainty: Certain

## backticks-usage [AL25]

The APKBUILD uses backticks for running a shell command, use `$()` instead.

Severity: Serious, Certainty: Possible

## function-keyword [AL9]

The APKBUILD uses the function keyword to declare a function. Use
*function()* instead.

the function keyword is a bashism. *abuild* uses Posix-compliant
shell with the *local* keyword.

Severity: Serious, Certainty: Certain

## space-before-function-parenthesis [AL10]

The APKBUILD has a space character between the name of a function
and the parenthesis that denote it is a function. Remove the superfluous
space.

Severity: Minor, Certainty: Certain

## space-after-function-parenthesis [AL11]

The APKBUILD doesn't have a space after the function parenthesis or has more
than one space. Use only one space after the function parenthesis.

Severity: Minor, Certainty: Certain

## newline-opening-brace [AL12]

The APKBUILD has a newline before the opening brace of a function. Put the
opening brace in the same line as the declaration with one space after the
function parenthesis.

Severity: Minor, Certainty: Certain

## superfluous-cd-builddir [AL13]

The APKBUILD has *cd "$builddir"* statements that are superfluous. Remove them.

Staring with v3.3.0 of *abuild* the *prepare*, *build*, *check* and *package*
functions automatically have their working directory set to the value of
*builddir*. It is also possible that there are 2 *cd "$builddir"* statements
one after the other.

Severity: Minor, Certainty: Possible

## pkgname-has-uppercase [AL14]

pkgname has uppercase characters, pkgname must have only lowercase characters.

More pkgnames can be added to the exceptions by passing a whitespace-separated
list in the variable CUSTOM_UPPERCASE_PKGNAMES.

Severity: Serious, Certainty: Certain

## pkgver-has-pkgrel [AL15]

pkgver has *-r* followed by a number, that is reserved for the relaease of a package
as defined by the *pkgrel* variable.

Severity: Serious, Certainty: Certain

## _builddir-is-set [AL26]

\_builddir is set instead of builddir, which is an old variable from before builddir
existed as a concept understood by abuild.

Severity: Serious, Certainty: Certain

## literal-integer-is-quoted [AL28]

A variable declaration containing only integers should not be quoted.

Severity: Minor,  Certainty: Certain

## pkgname-used-in-source [AL29]

"$pkgname" is used in the source url. This tightly couples the pkgname to the
upstream name, which makes it harder to rename packages or create specialized /
variants of packages.

Instead, use the upstream name fully written out. This only counts for the url
itself, not the local archive name prefix.

Severity: Minor, Certainty: Certain

## double-underscore-in-variable [AL30]

Usage of double underscore in variables is forbidden, use always one underscore
for variables that are not used by abuild.

Severity: Minor, Certainty: Certain

## variable-capitalized [AL31]

Variables should have no capitalized letters

Severity: Minor, Certainty: Certain

## braced-variable [AL32]

Variable has braces around it while it is not required, remove the braces.

Severity: Minor, Certainty: Possible

## cpan-variable [AL35]

The variables `cpandepends`, `cpanmakedepends` and `cpancheckdepends` were created
by the apkbuild-cpan program but are now deprecated and their contents must be merged
into the contents of its respective variable.

Severity: Minor, Certainty: Certain

## overwrite-xflags [AL36]

Don't overwrite `CFLAGS`, `GOFLAGS`, `CPPFLAGS`, `CXXFLAGS` and `FFLAGS`. There
flags are generally defined outside the of the APKBUILD and should not be
overwritten because they contain important flags that should not be discarded.
Instead, expand the variable. For example: `CFLAGS="$CFLAGS .."`

It is still possible to change the flags by using variable substitution. For
example: `CFLAGS="${CFLAGS/-Dflag}"`

This list might be expanded as more variables are found that should not be
overwritten.

Severity: Serious, Certainty: Certain

## invalid-option [AL49]

A option in the option= variable has a value that is not used by abuild,
while it most likely won't cause any problems it is considered good form
to remove it.

The variable VALID_CUSTOM_OPTIONS can be used to denote other options that
are acceptable, it takes a whitespace-separated list.

Severity: Minor, Certainty: Certain

## missing-default-prepare [AL54]

The prepare() function is defined but a call to default_prepare (which applies all
patches in source=) is missing. Please add default_prepare where appropriate in the
definition of prepare().

Severity: Serious, Certainty: Certain

## invalid-arch [AL57]

The variable 'arch' in the APKBUILD has an invalid value in it, the only options
are the name of the arches used by Alpine Linux and the strings 'noarch' and 'all'.

The acceptable arches are taken from /usr/share/abuild/functions.sh as those are the
ones recognized by abuild and thus Alpine Linux as valid arches, if you have other
arches, read below.

The variable CUSTOM_VALID_ARCHES can be used to denote other arches that are to be
considered valid, it takes a whitespace-separated list.

Severity: Serious, Certainty: Certain

## bad-version [AL61]

The pkgver matches a regex for a bad version, this means that the pkgver is a version
that is not desirable, like a testing/experimental branch.

The variable CUSTOM_BAD_VERSIONS can be used to add more pkgname@regexes... combinations,
separated by whitespace.

Severity: Important, Certainty: Certain

## volatile-source [AL62]

The given URL is volatile and will change checksum due to unexpected factors.

The most common example is GitHub pull-requests/GitLab Merge Requests which change as the
author pushes commits, another is GitHub commits which might change as GitHub changes their
version of Git in their infrastructure.

Severity: Serious, Certainty: Certain

# LABELS TAGS (initd-lint)

## unexpected-shebang-line [AL33]

OpenRC service files need to use `#!/sbin/openrc-run` to properly work. See
https://github.com/OpenRC/openrc/blob/master/service-script-guide.md#syntax-of-service-scripts
for more details.

Severity: Important, Certainty: Certain

## custom-start-stop-function [AL34]

It's discouraged to write custom start / stop function for service files. In
most cases it suffices to define `command`, `command_args`, and `pidfile`.  See
https://github.com/OpenRC/openrc/blob/master/service-script-guide.md#dont-write-your-own-startstop-functions
for more information.

Severity: Important, Certainty: Certain

# LABEL TAGS (secfixes-check)

## secfixes-missing-colon [AL37]

The secfixes header is missing a colon at the end.

Severity: Serious, Certainty: Certain

## pkgver-pkgrel-missing-colon [AL38]

The pkgver-pkgrel header is colon at the end.

Severity: Serious, Certainty: Certain

## pkgver-pkgrel-invalid-pkgver [AL39]

The pkgver-pkgrel header has an invalid pkgver.

Severity: Serious, Certainty: Certain

## pkgver-pkgrel-invalid-pkgrel [AL40]

The pkgver-pkgrel header has an invalid pkgrel.

Severity: Serious, Certainty: Certain

## security-identifier-missing-hyphen [AL41]

The security identifier is missing a leading hyphen.

Severity: Serious, Certainty: Certain

## security-identifier-wrong-indent [AL47]

The CVE identifier has too many or too few leading whitespaces,
it must have exactly 5 whitespaces between the comment marker and
the mapping hyphen.

Severity: Serious, Certainty: Certain

## pkgver-pkgrel-wrong-indent [AL48]

The pkgver-pkgrel header has too many or too few leading whitespaces,
it must have exactly 3 whitespaces between the comment marker and the
mapping hyphen.

Severity: Serious, Certainty: Certain

## cve-identifier-formatted-incorrectly [AL50]

The CVE identifier is not formatted correctly, please check the output
string for the reason why.

Severity: Minor, Certainty: Certain

## gnutls-sa-identifier-formatted-incorrectly [AL51]

The GNUTLS-SA identifier is not formatted correctly, please check the output
string for the reason why.

Severity: Minor, Certainty: Certain

## unknown-security-identifier [AL52]

An unknown identifier was passed, if it is a legitimate identifier then please
contact the authors to add support for it.

Severity: Minor, Certainty: Certain

## xsa-identifier-formatted-incorrectly [AL53]

The XSA identifier is not formatted correctly, please check the output string
for the reason why.

Severity: Minor, Certainty: Certain

## duplicate-identifier-value [AL59]

There are duplicate values of known identifiers. While this doesn't cause problems
while building it may cause false positives for users that rely on our secdb to
know if something is fixed.

Severity: Serious, Certainty: Certain

## ghsl-identifier-formatted-incorrectly [AL63]

The GHSL identifier is not formatted correctly, please check the output string
for the reason why.

Severity: Minor, Certainty: Certain
