#!/usr/bin/env bats

cmd=./initd-lint
service=$BATS_TMPDIR/service.initd

assert_match() {
	output=$1
	expected=$2

	echo "$output" | grep -qE "$expected"
}

@test 'warn about non-standard shebang' {
	cat <<-"EOF" >$service
	#!/bin/sh

	cmd_run="my_service"
	EOF

	run $cmd $service

	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "should use '#!/sbin/openrc-run'"
}

@test "don't warn about standard shebang" {
	cat <<-"EOF" >$service
	#!/sbin/openrc-run

	cmd_run="my_service"
	EOF

	run $cmd $service

	[[ $status -eq 0 ]]
}

@test "warn about custom start function" {
	cat <<-"EOF" >$service
	#!/sbin/openrc-run

	start() {
		start_command
	}
	EOF

	run $cmd $service

	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "custom start/stop function"
}

@test "warn about custom stop function" {
	cat <<-"EOF" >$service
	#!/sbin/openrc-run

	stop () {
		stop_command
	}
	EOF

	run $cmd $service

	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "custom start/stop function"
}

@test "should not warn about pre_start" {
	cat <<-"EOF" >$service
	#!/sbin/openrc-run

	pre_start() {
		start_command
	}
	EOF

	run $cmd $service

	[[ $status -eq 0 ]]
}
